<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Entity;

use Drupal\Core\Entity\EntityType;
use Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage;

/**
 * Provides an implementation of a cache entity type and its metadata.
 *
 * @package Drupal\cache_entity_type\Entity
 */
class CacheEntityType extends EntityType {

  /**
   * {@inheritdoc}
   */
  public function __construct($definition) {
    parent::__construct($definition);

    $this->handlers += [
      'storage' => 'Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyKey() {
    return 'cache_entity';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   *   If the provided class does not implement CacheEntityStorage.
   *
   * @see \Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage
   *
   * @phpstan-ignore-next-line
   */
  protected function checkStorageClass($class) {
    $requiredClass = CacheEntityStorage::class;
    if (!is_subclass_of($class, $requiredClass)) {
      throw new \InvalidArgumentException("$class does not implement $requiredClass");
    }
  }

}
