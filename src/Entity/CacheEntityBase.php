<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Entity;

use Drupal\Core\Entity\EntityBase;

/**
 * Base class for entities that are stored in the Drupal cache.
 *
 * Example configuration for an entity:
 * @code
 * (@)EntityType(
 *   id = "my_module_daily_weather_forecast",
 *   label = @Translation("Weather forecast for a single day"),
 *   handlers = {
 *     "storage" = "Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage",
 *   },
 *   render_cache = FALSE,
 *   entity_keys = {
 *     "id" = "id"
 *   },
 * )
 * @endcode
 *
 * @package Drupal\cache_entity_type\Entity
 */
abstract class CacheEntityBase extends EntityBase implements CacheEntityInterface {

  /**
   * The key of the entity ID property.
   */
  public const ID_KEY = 'id';

  /**
   * Holds untranslatable entity keys such as the ID, bundle, and revision ID.
   *
   * @var array
   */
  protected array $entityKeys;

  /**
   * CacheEntityBase an Entity object.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   *   If the entity type has bundles,
   *   the bundle key has to be specified.
   * @param string $entity_type
   *   The type of the entity to create.
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);

    $this->entityKeys = ['id' => static::ID_KEY];
  }

}
