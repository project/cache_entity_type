<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Entity\Cache;

/**
 * Class CacheBinNameFactory.
 *
 * @package Drupal\cache_entity_type\Entity\Cache
 */
class CacheBinNameFactory {

  /**
   * Only static calls allowed.
   */
  private function __construct() {}

  /**
   * Returns the cache bin name for given entity type ID.
   *
   * @param string $entityTypeId
   *   Entity type Id.
   *
   * @return string
   *   Cache bin name.
   */
  public static function binName(string $entityTypeId): string {
    return 'cache_entity.' . $entityTypeId;
  }

  /**
   * Returns the cache bin service name for given entity type ID.
   *
   * @param string $entityTypeId
   *   Entity type Id.
   *
   * @return string
   *   Cache bin service name.
   */
  public static function binServiceName(string $entityTypeId): string {
    return 'cache.' . self::binName($entityTypeId);
  }

}
