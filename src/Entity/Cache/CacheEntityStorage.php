<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Entity\Cache;

use Drupal\cache_entity_type\Exception\IdHashGenerationException;
use Drupal\cache_entity_type\Utility\DiffArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\cache_entity_type\Entity\CacheEntityInterface;
use Drupal\cache_entity_type\Exception\UnsupportedEntityTypeCacheStorageException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a cache storage backend for entities.
 *
 * TODO: Revisions are currently not supported.
 *
 * @package Drupal\cache_entity_type\Entity\Cache
 */
class CacheEntityStorage extends EntityStorageBase {

  /**
   * The cache backend used to store entities.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBackend;

  /**
   * The cache ID of the cache entry that contains the ID map.
   *
   * @var string
   */
  protected string $idMapCacheId;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  /**
   * Constructs a new CacheEntityStorage.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type.
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cacheFactory
   *   The cache factory.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memoryCache
   *   The memory cache.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   The cache tags invalidator.
   */
  public function __construct(EntityTypeInterface $entityType, CacheFactoryInterface $cacheFactory, MemoryCacheInterface $memoryCache, LoggerChannelFactoryInterface $loggerChannelFactory, CacheTagsInvalidatorInterface $cacheTagsInvalidator) {
    parent::__construct($entityType, $memoryCache);
    $this->cacheBackend = $cacheFactory->get(CacheBinNameFactory::binName($this->entityTypeId));
    $this->logger = $loggerChannelFactory->get('cache_entity_storage');
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;

    $this->idMapCacheId = $this->getCacheIdPrefix($this->entityTypeId) . '.id_mapping';
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('cache_factory'),
      $container->get('entity.memory_cache'),
      $container->get('logger.factory'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * Returns the prefix of all cache IDs for the current entity type.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   *
   * @return string
   *   The cache ID prefix.
   */
  private function getCacheIdPrefix(string $entityTypeId): string {
    return 'cache_entity.' . $entityTypeId;
  }

  /**
   * Checks if given cache entry is valid.
   *
   * @param object|bool $cacheEntry
   *   The cache entry.
   *   Instance of \stdClass or FALSE.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  private function isCacheEntryValid($cacheEntry): bool {
    return $cacheEntry !== FALSE && isset($cacheEntry->data) && isset($cacheEntry->valid) && $cacheEntry->valid === TRUE;
  }

  /**
   * Returns the entity contained inside given cache entry.
   *
   * @param object|bool $cacheEntry
   *   The cache entry.
   *   Instance of \stdClass or FALSE.
   *
   * @return \Drupal\cache_entity_type\Entity\CacheEntityInterface|null
   *   The entity or NULL if none is contained.
   */
  private function extractEntityFromCacheEntry($cacheEntry): ?CacheEntityInterface {
    /* PHPStan doesn't recognize that it's an object
    if isCacheEntryValid is TRUE.
    And adding the @var typehint after the isCacheEntryValid call doesn't work. */
    /** @var \stdClass $cacheEntry */
    if (
      $this->isCacheEntryValid($cacheEntry)
      && $cacheEntry->data instanceof CacheEntityInterface
      /* Also check via method to allow child storages to restrict supported types. */
      && $this->isSupportedEntityType($cacheEntry->data)
    ) {
      return $cacheEntry->data;
    }

    return NULL;
  }

  /**
   * Returns the entities contained inside given cache entry array.
   *
   * @param object[] $cacheEntries
   *   Instances of \stdClass.
   *
   * @return \Drupal\cache_entity_type\Entity\CacheEntityInterface[]
   *   Associative array of entities, keyed by the entity IDs.
   */
  private function extractEntitiesFromCacheEntries(array $cacheEntries): array {
    $entities = [];

    foreach ($cacheEntries as $cacheEntry) {
      $entity = $this->extractEntityFromCacheEntry($cacheEntry);
      if ($entity instanceof CacheEntityInterface) {
        $entities[$entity->id()] = $entity;
      }
    }

    return $entities;
  }

  /**
   * Whether or not to log failing to load missing entities via fallback source.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  protected function shouldLogFallbackEntityLoadingFailure(): bool {
    return FALSE;
  }

  /**
   * Gets entities that are not stored from the fallback data source.
   *
   * For example an API endpoint,
   * files on the server, ...
   * Entrypoint for extending child storages.
   *
   * May return more entities than requested.
   * E.g. when an API endpoint returns all entities,
   * it makes more sense to store all entities at once
   * when a single one is missing.
   * Instead of continuously calling the API for each entity.
   *
   * @param int[]|null $entityIds
   *   (optional) Entity IDs of the entities that are missing.
   *   Load all available entities if NULL.
   *   Depending on the fallback data source
   *   loading all might not be supported.
   *   E.g. if you only have an API endpoint to load by ID available,
   *   but none to get all.
   *   Defaults to NULL.
   *
   * @return \Drupal\cache_entity_type\Entity\CacheEntityInterface[]
   *   Associative array of entities, keyed by the entity IDs (int).
   *   May be empty. Then entities have to be stored explicitly in the storage.
   */
  protected function loadMissingEntitiesFromFallbackSource(array $entityIds = NULL): array {
    return [];
  }

  /**
   * Loads and saves missing entities from the fallback source.
   *
   * @param int[]|null $missingEntityIds
   *   (optional) Entity IDs of the entities that are missing.
   *   Load all available entities if NULL.
   *   Depending on the fallback data source
   *   loading all might not be supported.
   *   E.g. if you only have an API endpoint to load by ID available,
   *   but none to get all.
   *   Defaults to NULL.
   *
   * @return \Drupal\cache_entity_type\Entity\CacheEntityInterface[]
   *   All loaded entities if loading all ($missingEntityIds === NULL).
   *   Or the entities with the given IDs.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures when saving entities.
   */
  private function loadAndSaveEntitiesFromFallbackSource(array $missingEntityIds = NULL): array {
    $fallbackLoadedEntities = $this->loadMissingEntitiesFromFallbackSource($missingEntityIds);
    /* We do not call ::save() here
    because we want to skip the ::doPreSave()logic
    that checks if the entity already exists.
    Otherwise we would get "entity with ID XY already exists." exceptions.
    We overwrite any entities, disregarding if they already exist or not. */
    $this->forceSaveMultiple($fallbackLoadedEntities);

    $isLoadingAll = $missingEntityIds === NULL;
    if ($isLoadingAll) {
      return $fallbackLoadedEntities;
    }

    if ($this->shouldLogFallbackEntityLoadingFailure() && !DiffArray::arrayKeysExist($missingEntityIds, $fallbackLoadedEntities)) {
      $this->logger->error(
        'Failed to load entities with IDs "@entityIds" from fallback data source.',
        ['@entityIds' => implode(', ', $missingEntityIds)]
      );
    }

    /* Make sure that only the requested entities are returned.
    In case the ::loadMissingEntitiesFromFallbackSource() method loads more entities than requested. */
    $queriedFallbackLoadedEntities = [];
    foreach ($fallbackLoadedEntities as $loadedEntity) {
      if (in_array($loadedEntity->id(), $missingEntityIds)) {
        $queriedFallbackLoadedEntities[$loadedEntity->id()] = $loadedEntity;
      }
    }

    return $queriedFallbackLoadedEntities;
  }

  /**
   * Loads all entities that are stored.
   *
   * @param bool $useFallbackLoading
   *   (optional) Trigger the fallback loading to also include
   *   all entities that are available in the fallback
   *   data source, but currently not saved in the entity storage.
   *   Defaults to TRUE.
   *
   * @return \Drupal\cache_entity_type\Entity\CacheEntityInterface[]
   *   Associative array of entities, keyed by the entity IDs.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures when saving entities.
   */
  protected function loadAll(bool $useFallbackLoading = TRUE): array {
    if ($useFallbackLoading) {
      $this->loadAndSaveEntitiesFromFallbackSource();
    }

    $cacheIds = $this->getIdMap()->getAll();
    $cacheEntries = $this->cacheBackend->getMultiple($cacheIds);

    return $this->extractEntitiesFromCacheEntries($cacheEntries);
  }

  /**
   * Loads the entities with the given IDs.
   *
   * @param int[] $entityIds
   *   An array of entity IDs.
   *
   * @return \Drupal\cache_entity_type\Entity\CacheEntityInterface[]
   *   Associative array of entities, keyed by the entity IDs.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures when saving fallback loaded entities.
   */
  protected function loadWithIds(array $entityIds): array {
    $idMap = $this->getIdMap();
    $missingEntityIds = [];
    $alreadyStoredEntityIds = [];

    foreach ($entityIds as $id) {
      if ($idMap->has($id)) {
        $alreadyStoredEntityIds[] = $id;
      }
      else {
        $missingEntityIds[] = $id;
      }
    }

    $cacheIds = $idMap->getMultipleCacheIds($alreadyStoredEntityIds);
    $cacheEntries = $this->cacheBackend->getMultiple($cacheIds);

    /* Successfully loaded cids will be removed from
    $cacheIds by $this->cacheBackend->getMultiple().
    So any left over cids indicate that these were not in the cache. */
    $notFoundInCacheEntityIds = $cacheIds;
    if (!empty($notFoundInCacheEntityIds)) {
      foreach ($notFoundInCacheEntityIds as $missingEntityCacheId) {
        $missingEntityId = $idMap->getEntityId($missingEntityCacheId);
        if ($missingEntityId !== NULL) {
          $missingEntityIds[] = $missingEntityId;
        }
        else {
          // TODO: Log error, this is not a correct ID Map state. Do something about it (delete cache) or just log?
        }
      }
    }

    // Try to load entities that are missing from the fallback data source.
    if (!empty($missingEntityIds)) {
      $queriedFallbackLoadedEntities = $this->loadAndSaveEntitiesFromFallbackSource($missingEntityIds);
    }
    else {
      $queriedFallbackLoadedEntities = [];
    }

    // Use union instead of array_merge(), to preserve int keys (entity IDs).
    return $this->extractEntitiesFromCacheEntries($cacheEntries) + $queriedFallbackLoadedEntities;
  }

  /**
   * Performs storage-specific loading of entities.
   *
   * @param array|null $ids
   *   (optional) An array of entity IDs, or NULL to load all entities.
   *
   * @return \Drupal\cache_entity_type\Entity\CacheEntityInterface[]
   *   Associative array of entities, keyed by the entity IDs.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures when saving fallback loaded entities.
   */
  protected function doLoadMultiple(array $ids = NULL) {
    $shouldLoadAll = empty($ids);
    if ($shouldLoadAll) {
      return $this->loadAll();
    }
    else {
      return $this->loadWithIds($ids);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadRevision($revision_id) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore-next-line
   */
  public function deleteRevision($revision_id) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore-next-line
   */
  protected function doDelete($entities) {
    // We can expect that the keys are equal to the entity IDs.
    $entityIds = array_keys($entities);
    $idMap = $this->getIdMap();

    $cacheIds = $idMap->getMultipleCacheIds($entityIds);
    $idMap->deleteMultiple($entityIds);
    $this->storeIdMap($idMap);

    $this->cacheBackend->deleteMultiple($cacheIds);
  }

  /**
   * Sets the id property on given entity if it is not set yet.
   *
   * @param \Drupal\cache_entity_type\Entity\CacheEntityInterface $entity
   *   The entity.
   * @param int $entityId
   *   The id to set.
   */
  private function setIdPropertyOnEntityIfNotSet(CacheEntityInterface $entity, int $entityId): void {
    if (isset($this->idKey)) {
      if (!isset($entity->{$this->idKey})) {
        $entity->{$this->idKey} = $entityId;
      }
    }
    else {
      $this->logger->error('The "idKey" property is not set. Cannot set "id" property on entity being saved.');
    }
  }

  /**
   * Checks if given entity is supported by this storage handler.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  protected function isSupportedEntityType(EntityInterface $entity): bool {
    if ($entity instanceof CacheEntityInterface) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Checks if given entity is supported by this storage handler.
   *
   * Throws an exception if not.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param string $message
   *   The exception message.
   *   Optional.
   *
   * @throws \Drupal\cache_entity_type\Exception\UnsupportedEntityTypeCacheStorageException
   *   If entity type is not supported.
   */
  private function throwExceptionIfNotSupportedEntityType(EntityInterface $entity, string $message = ''): void {
    if (!$this->isSupportedEntityType($entity)) {
      if ($message !== '') {
        throw new UnsupportedEntityTypeCacheStorageException($message);
      }
      else {
        throw new UnsupportedEntityTypeCacheStorageException();
      }
    }
  }

  /**
   * Performs storage-specific saving of the entity.
   *
   * @param int|string|null $id
   *   The original entity ID.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to save.
   *
   * @return bool|int
   *   If the record insert or update failed, returns FALSE. If it succeeded,
   *   returns SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   *
   * @throws \Drupal\cache_entity_type\Exception\UnsupportedEntityTypeCacheStorageException
   *   If entity type is not supported.
   * @throws \Drupal\cache_entity_type\Exception\CacheEntityIdGenerationException
   *   On unexpected error during ID generation.
   */
  protected function doSave($id, EntityInterface $entity) {
    $this->throwExceptionIfNotSupportedEntityType($entity);

    /** @var \Drupal\cache_entity_type\Entity\CacheEntityInterface $entity */
    if ($this->has($id, $entity)) {
      $operation = SAVED_UPDATED;
      $entityId = (int) $id;
    }
    else {
      $operation = SAVED_NEW;

      if (is_int($id) && $id > 0) {
        $entityId = $id;
      }
      else {
        $entityId = $this->getIdMap()->generateNextId();
      }
      $this->setIdPropertyOnEntityIfNotSet($entity, $entityId);
    }

    $cacheId = $this->buildStorageCacheId($entityId);

    $idMap = $this->getIdMap();
    $idMap->set($entityId, $cacheId);
    $this->storeIdMap($idMap);

    $this->cacheBackend->set(
      $cacheId,
      $entity,
      Cache::PERMANENT,
      [$cacheId]
    );

    return $operation;
  }

  /**
   * Force-saves the given entity.
   *
   * Overwriting existing if an entity with the same ID is already stored.
   *
   * @param \Drupal\cache_entity_type\Entity\CacheEntityInterface $entity
   *   The entity to save.
   *
   * @throws \Drupal\cache_entity_type\Exception\CacheEntityIdGenerationException
   *   On unexpected error during ID generation.
   * @throws \Drupal\cache_entity_type\Exception\UnsupportedEntityTypeCacheStorageException
   *   If entity type is not supported.
   */
  protected function forceSave(CacheEntityInterface $entity): void {
    // Allow code to run before saving.
    $entity->preSave($this);
    $this->invokeHook('presave', $entity);

    $this->doSave($entity->id(), $entity);
    $this->doPostSave($entity, TRUE);
  }

  /**
   * Force-saves the given entities.
   *
   * Overwriting existing if an entity with the same ID is already stored.
   *
   * @param \Drupal\cache_entity_type\Entity\CacheEntityInterface[] $entities
   *   The entities to save.
   *
   * @throws \Drupal\cache_entity_type\Exception\CacheEntityIdGenerationException
   *   On unexpected error during ID generation.
   * @throws \Drupal\cache_entity_type\Exception\UnsupportedEntityTypeCacheStorageException
   *   If entity type is not supported.
   */
  protected function forceSaveMultiple(array $entities): void {
    foreach ($entities as $entity) {
      $this->forceSave($entity);
    }
  }

  /**
   * Builds the cache ID that identifies the entity object with given ID.
   *
   * @param int $entityId
   *   The entity ID.
   *
   * @return string
   *   The cache ID.
   */
  private function buildStorageCacheId(int $entityId): string {
    return $this->getCacheIdPrefix($this->entityTypeId) . '.' . $entityId;
  }

  /**
   * Determines if this entity already exists in storage.
   *
   * @param int|string|null $id
   *   The original entity ID.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  protected function has($id, EntityInterface $entity) {
    if ($id === NULL) {
      return FALSE;
    }

    $id = (int) $id;
    return $this->getIdMap()->has($id);
  }

  /**
   * {@inheritdoc}
   */
  public function hasData() {
    return $this->getIdMap()->isEmpty();
  }

  /**
   * Returns the ID map.
   *
   * @return \Drupal\cache_entity_type\Entity\Cache\IdToCacheIdMap
   *   The ID map.
   */
  private function getIdMap(): IdToCacheIdMap {
    /* TODO: Store in memory cache/as static property?
    Might cause ID map inconsistencies due to parallel requests? */
    $cacheEntry = $this->cacheBackend->get($this->idMapCacheId);

    /* PHPStan doesn't recognize that it's an object
    if isCacheEntryValid is TRUE.
    And adding the @var typehint after the isCacheEntryValid call doesn't work. */
    /** @var \stdClass $cacheEntry */
    if ($this->isCacheEntryValid($cacheEntry) && $cacheEntry->data instanceof IdToCacheIdMap) {
      return $cacheEntry->data;
    }

    return $this->createAndStoreNewIdMap();
  }

  /**
   * Creates a new ID map and store it inside the cache.
   *
   * @return \Drupal\cache_entity_type\Entity\Cache\IdToCacheIdMap
   *   The ID map.
   */
  private function createAndStoreNewIdMap(): IdToCacheIdMap {
    $idMap = IdToCacheIdMap::create();
    $this->storeIdMap($idMap);

    return $idMap;
  }

  /**
   * Stores the given ID map in the cache.
   *
   * @param \Drupal\cache_entity_type\Entity\Cache\IdToCacheIdMap $idMap
   *   The ID map to store.
   */
  private function storeIdMap(IdToCacheIdMap $idMap): void {
    $this->cacheBackend->set($this->idMapCacheId, $idMap, Cache::PERMANENT, [$this->idMapCacheId]);
  }

  /**
   * Invalidates all cache entries.
   */
  public function invalidateAll(): void {
    $this->cacheBackend->invalidateAll();
  }

  /**
   * {@inheritdoc}
   *
   * TODO: Implement.
   */
  protected function getQueryServiceName() {
    return 'entity.query.null';
  }

  /**
   * Generates an unique int entity ID from given string.
   *
   * @param string $idString
   *   An unique string that should be converted to an int.
   *
   * @return int
   *   The ID.
   *
   * @throws \Drupal\cache_entity_type\Exception\IdHashGenerationException
   *   Thrown if it was not possible to generate a valid ID hash from a string.
   */
  protected function generateIntIdFromString(string $idString): int {
    // See https://www.php.net/manual/en/function.crc32.php.
    $hexHash = hash('crc32b', $idString);
    $intHash = hexdec($hexHash);
    if (!is_numeric($intHash)) {
      throw new IdHashGenerationException($idString, $hexHash);
    }

    if ($intHash < 0) {
      $this->logger->warning(
        'The integer ID that was generated from ID string "@idString" is negative. This can happen on 32bit systems. The integer was converted to a positive number.',
        ['@idString' => $idString]
      );
    }
    /* Make sure it's always positive.
    Since negative IDs are not valid.
    32bit systems could generate negative integers.
    We take the risk of possible collisions. */
    return (int) abs($intHash);
  }

}
