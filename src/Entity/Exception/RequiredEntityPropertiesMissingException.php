<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Exception;

use Drupal\Core\Entity\EntityMalformedException;

/**
 * Thrown if required entity properties are not set when constructing an entity.
 *
 * @package Drupal\cache_entity_type\Entity\Exception
 */
class RequiredEntityPropertiesMissingException extends EntityMalformedException {

  /**
   * RequiredEntityPropertiesMissingException constructor.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   * @param array $values
   *   The values used to create a new entity.
   */
  public function __construct(string $entityTypeId, array $values) {
    $message = 'Required entity properties are missing. Entity Type ID: "' . $entityTypeId . '". Values: "' . print_r($values, TRUE) . '".';

    parent::__construct($message, 0, NULL);
  }

}
