<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Exception;

use Drupal\Core\Entity\EntityStorageException;

/**
 * Thrown if it was not possible to generate a valid ID hash from a string.
 *
 * @package Drupal\cache_entity_type\Entity\Exception
 */
class IdHashGenerationException extends EntityStorageException {

  /**
   * IdHashGenerationException constructor.
   *
   * @param string $idString
   *   The ID string.
   * @param string $generatedHash
   *   The generated hash that's not a number.
   */
  public function __construct(string $idString, string $generatedHash) {
    $message = 'Could not generate a int ID from given ID string. ID string: "' . $idString . '", resulting hash: "' . $generatedHash . '".';

    parent::__construct($message, 0, NULL);
  }

}
