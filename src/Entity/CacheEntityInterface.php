<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Entity;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a common interface for cache entity objects.
 *
 * @ingroup entity_api
 * @package Drupal\cache_entity_type\Entity
 */
interface CacheEntityInterface extends EntityInterface {}
