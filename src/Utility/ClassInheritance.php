<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Utility;

/**
 * Provides checks to validate if a class extends another.
 *
 * @package Drupal\cache_entity_type\Utility
 */
class ClassInheritance {

  /**
   * Checks if classes are the same or child class extends parent class.
   *
   * @param string $childClassName
   *   The fully qualified child class name.
   * @param string $parentClassName
   *   The fully qualified parent class name.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public static function isSameOrDoesExtend(string $childClassName, string $parentClassName): bool {
    if ($childClassName === $parentClassName) {
      return TRUE;
    }

    return is_subclass_of($childClassName, $parentClassName, TRUE);
  }

}
