<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Utility;

/**
 * Utility for comparing arrays.
 *
 * @package Drupal\cache_entity_type\Utility
 */
class DiffArray {

  /**
   * Check if given keys exist in array.
   *
   * @param array $keys
   *   The keys to look for.
   * @param array $array
   *   The array to check.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public static function arrayKeysExist(array $keys, array $array): bool {
    return !array_diff_key(array_flip($keys), $array);
  }

}
