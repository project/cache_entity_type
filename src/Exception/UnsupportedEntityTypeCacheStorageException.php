<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Exception;

use Drupal\Core\Entity\EntityStorageException;

/**
 * Class UnsupportedEntityTypeCacheStorageException.
 *
 * @package Drupal\cache_entity_type\Exception
 */
class UnsupportedEntityTypeCacheStorageException extends EntityStorageException {

  /**
   * Construct the error object.
   * @link https://php.net/manual/en/error.construct.php
   *
   * @param string $message
   *   [optional] The Error message to throw.
   */
  public function __construct($message = 'Received entity of unsupported type.') {
    parent::__construct($message, 0, NULL);
  }

}
