<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Exception;

use Drupal\Core\Entity\EntityStorageException;

/**
 * Class InvalidEntityIdException.
 *
 * @package Drupal\cache_entity_type\Exception
 */
class InvalidEntityIdException extends EntityStorageException {

  /**
   * InvalidEntityIdException constructor.
   *
   * @param mixed $entityIdId
   *   The entity ID.
   */
  public function __construct($entityIdId) {
    $message = 'Cache entity ID is invalid. Got value: "' . print_r($entityIdId, TRUE) . '"';

    parent::__construct($message, 0, NULL);
  }

}
