<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type\Exception;

use Drupal\Core\Entity\EntityStorageException;

/**
 * Class CacheEntityIdGenerationException.
 *
 * @package Drupal\cache_entity_type\Exception
 */
class CacheEntityIdGenerationException extends EntityStorageException {

  /**
   * CacheEntityIdGenerationException constructor.
   */
  public function __construct() {
    parent::__construct('Could not generate a new cache entity ID.', 0, NULL);
  }

}
