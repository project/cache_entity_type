<?php

declare(strict_types=1);

namespace Drupal\Tests\cache_entity_type\Unit\Entity\Cache;

use Drupal\cache_entity_type\Entity\Cache\IdToCacheIdMap;
use Drupal\cache_entity_type\Exception\InvalidEntityIdException;
use Drupal\Tests\UnitTestCase;

/**
 * Testing class to test the LinkHandler.
 *
 * @package Drupal\Tests\cache_entity_type\Unit\Entity\Cache
 * @coversDefaultClass \Drupal\cache_entity_type\Entity\Cache\IdToCacheIdMap
 * @group cache_entity_type
 */
class IdToCacheIdMapTest extends UnitTestCase {

  /**
   * Provides test data for next ID generation.
   */
  public function providerGenerateNextId(): \Iterator {
    yield [
      'idMap' => IdToCacheIdMap::create([]),
      'expectedId' => 1
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        2 => 'cache_id.2'
      ]),
      'expectedId' => 1
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        5 => 'cache_id.5'
      ]),
      'expectedId' => 1
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        5 => 'cache_id.5',
        6 => 'cache_id.6',
        7 => 'cache_id.7'
      ]),
      'expectedId' => 1
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        1 => 'cache_id.1'
      ]),
      'expectedId' => 2
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        1 => 'cache_id.1',
        2 => 'cache_id.2',
        3 => 'cache_id.3',
        4 => 'cache_id.4'
      ]),
      'expectedId' => 5
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        1 => 'cache_id.1',
        2 => 'cache_id.2',
        3 => 'cache_id.3',
        70 => 'cache_id.70'
      ]),
      'expectedId' => 4
    ];
  }

  /**
   * Tests next ID generation.
   *
   * @param \Drupal\cache_entity_type\Entity\Cache\IdToCacheIdMap $idMap
   *   ID map.
   * @param int $expectedId
   *   The expected next entity ID.
   *
   * @throws \Drupal\cache_entity_type\Exception\CacheEntityIdGenerationException
   *   On unexpected error.
   *
   * @dataProvider providerGenerateNextId
   * @covers ::generateNextId
   */
  public function testGenerateNextId(IdToCacheIdMap $idMap, int $expectedId): void {
    $this->assertEquals($expectedId, $idMap->generateNextId());
  }

  /**
   * Provides test data for multiple item deletion.
   */
  public function providerDeleteMultiple(): \Iterator {
    yield [
      'idMap' => IdToCacheIdMap::create([]),
      'idsToDelete' => [],
      'expectedMap' => []
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        1 => 'cache_id.1',
      ]),
      'idsToDelete' => [2],
      'expectedMap' => [
        1 => 'cache_id.1'
      ]
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        1 => 'cache_id.1',
      ]),
      'idsToDelete' => [1],
      'expectedMap' => []
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        1 => 'cache_id.1',
        2 => 'cache_id.2',
        3 => 'cache_id.3',
      ]),
      'idsToDelete' => [1, 2, 3],
      'expectedMap' => []
    ];

    yield [
      'idMap' => IdToCacheIdMap::create([
        1 => 'cache_id.1',
        2 => 'cache_id.2',
        3 => 'cache_id.3',
      ]),
      'idsToDelete' => [2],
      'expectedMap' => [
        1 => 'cache_id.1',
        3 => 'cache_id.3',
      ]
    ];
  }

  /**
   * Tests multiple item deletion.
   *
   * @param \Drupal\cache_entity_type\Entity\Cache\IdToCacheIdMap $idMap
   *   ID map.
   * @param array $idsToDelete
   *   The expected next entity ID.
   * @param string[] $expectedMap
   *   The expected resulting ID map.
   *
   * @dataProvider providerDeleteMultiple
   * @covers ::deleteMultiple
   */
  public function testDeleteMultiple(IdToCacheIdMap $idMap, array $idsToDelete, array $expectedMap): void {
    $idMap->deleteMultiple($idsToDelete);
    $this->assertEquals($expectedMap, $idMap->getAll());
  }

  /**
   * Tests multiple item deletion with invalid entity IDs argument.
   *
   * @covers ::deleteMultiple
   */
  public function testDeleteMultipleTypeException(): void {
    $idMap = IdToCacheIdMap::create();

    $this->expectException(InvalidEntityIdException::class);
    /* We consciously call it with invalid parameter type.
    @phpstan-ignore-next-line */
    $idMap->deleteMultiple(['not an int']);
  }

}
