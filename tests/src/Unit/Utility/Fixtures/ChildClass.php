<?php

declare(strict_types=1);

namespace Drupal\Tests\cache_entity_type\Unit\Utility\Fixtures;

/**
 * Class ChildClass.
 *
 * @package Drupal\Tests\cache_entity_type\Utility
 */
class ChildClass extends ParentClass {}
