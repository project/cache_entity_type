<?php

declare(strict_types=1);

namespace Drupal\Tests\cache_entity_type\Unit\Utility\Fixtures;

/**
 * Class ParentClass.
 *
 * @package Drupal\Tests\cache_entity_type\Utility\Fixtures
 */
class ParentClass {}
