<?php

declare(strict_types=1);

namespace Drupal\Tests\cache_entity_type\Unit\Utility;

use Drupal\cache_entity_type\Utility\ClassInheritance;
use Drupal\Tests\cache_entity_type\Unit\Utility\Fixtures\ChildClass;
use Drupal\Tests\cache_entity_type\Unit\Utility\Fixtures\ParentClass;
use Drupal\Tests\cache_entity_type\Unit\Utility\Fixtures\SomeOtherClass;
use Drupal\Tests\UnitTestCase;

/**
 * Tests ClassInheritance.
 *
 * @package Drupal\Tests\cache_entity_type\Utility
 * @coversDefaultClass \Drupal\cache_entity_type\Utility\ClassInheritance
 * @group cache_entity_type
 */
class ClassInheritanceTest extends UnitTestCase {

  /**
   * @covers ::isSameOrDoesExtend
   */
  public function testIsSameOrDoesExtend(): void {
    static::assertTrue(
      ClassInheritance::isSameOrDoesExtend(
        ChildClass::class,
        ParentClass::class
      )
    );

    static::assertTrue(
      ClassInheritance::isSameOrDoesExtend(
        ChildClass::class,
        ChildClass::class
      )
    );

    static::assertTrue(
      ClassInheritance::isSameOrDoesExtend(
        ParentClass::class,
        ParentClass::class
      )
    );

    static::assertFalse(
      ClassInheritance::isSameOrDoesExtend(
        SomeOtherClass::class,
        ParentClass::class
      )
    );
  }

}
