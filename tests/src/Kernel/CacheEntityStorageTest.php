<?php

declare(strict_types=1);

namespace Drupal\Tests\cache_entity_type\Kernel;

use Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage;
use Drupal\cache_entity_type_example\Entity\DailyWeatherForecast;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Tests the CacheEntityStorage class.
 *
 * @package Drupal\Tests\cache_entity_type\Kernel
 * @coversDefaultClass \Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage
 * @group cache_entity_type
 */
class CacheEntityStorageTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'cache_entity_type_example',
  ];

  private const ENTITY_TYPE_ID = 'example_daily_weather_forecast';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema(self::ENTITY_TYPE_ID);

    $entityTypeManager = $this->container->get('entity_type.manager');
    if (!$entityTypeManager instanceof EntityTypeManagerInterface) {
      throw new ServiceNotFoundException('entity_type.manager');
    }
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Checks if the storage can be loaded.
   *
   * @covers ::createInstance
   */
  public function testStorageCanBeLoaded(): void {
    $storage = NULL;

    try {
      $storage = $this->entityTypeManager->getStorage(self::ENTITY_TYPE_ID);
    }
    catch (\Exception $exception) {
      $this->fail('Getting cache entity type storage failed.');
    }

    $this->assertInstanceOf(CacheEntityStorage::class, $storage);
  }

  /**
   * @covers ::doSave
   */
  public function testEntityCreationAndSavingAndPropertyGetting(): void {
    $storage = $this->entityTypeManager->getStorage(self::ENTITY_TYPE_ID);

    $entity1 = $storage->create([
      'key' => 'forecast_st_gallen_2020_09_15',
      'averageTemperature' => 25.3
    ]);

    $this->assertInstanceOf(DailyWeatherForecast::class, $entity1);
    /** @var \Drupal\cache_entity_type_example\Entity\DailyWeatherForecast $entity1 */
    self::assertEquals('forecast_st_gallen_2020_09_15', $entity1->getKey());
    self::assertEqualsWithDelta(25.3, $entity1->getAverageTemperature(), 0.1, 'Entity value is not the same as passed to ::create.');

    try {
      $entity1->save();
    }
    catch (EntityStorageException $entityStorageException) {
      $this->fail('Saving entity threw an EntityStorageException.');
    }

    // Check that saving does not change any values.
    $this->assertInstanceOf(DailyWeatherForecast::class, $entity1);
    self::assertEquals('forecast_st_gallen_2020_09_15', $entity1->getKey());
    self::assertEqualsWithDelta(25.3, $entity1->getAverageTemperature(), 0.1, 'Entity value is not the same as passed to ::create.');
  }

  /**
   * Asserts that two DailyWeatherForecast entities are the same one.
   *
   * @param \Drupal\cache_entity_type_example\Entity\DailyWeatherForecast $expectedEntity
   *   The expected entity.
   * @param mixed $actualEntity
   *   The actual entity.
   */
  protected function assertIsEntityEqual(DailyWeatherForecast $expectedEntity, $actualEntity): void {
    $this->assertInstanceOf(DailyWeatherForecast::class, $actualEntity);

    /** @var \Drupal\cache_entity_type_example\Entity\DailyWeatherForecast $actualEntity */
    self::assertEquals($expectedEntity->getKey(), $actualEntity->getKey());
    self::assertEqualsWithDelta($expectedEntity->getAverageTemperature(), $actualEntity->getAverageTemperature(), 0.1);
    self::assertEquals($expectedEntity->id(), $actualEntity->id());
  }

  /**
   * @covers ::doSave
   * @covers ::doLoadMultiple
   */
  public function testCrud(): void {
    $storage = $this->entityTypeManager->getStorage(self::ENTITY_TYPE_ID);

    $entity1 = $storage->create([
      'key' => 'forecast_st_gallen_2020_09_20',
      'averageTemperature' => 25.3
    ]);
    $entity1->save();
    $this->assertInstanceOf(DailyWeatherForecast::class, $entity1);
    self::assertEquals(1, $entity1->id());

    $entity2 = $storage->create([
      'key' => 'forecast_st_gallen_2020_09_21',
      'averageTemperature' => 23.6
    ]);
    $entity2->save();
    $this->assertInstanceOf(DailyWeatherForecast::class, $entity2);
    self::assertEquals(2, $entity2->id());

    $entity3 = $storage->create([
      'key' => 'forecast_st_gallen_2020_09_22',
      'averageTemperature' => 21.1
    ]);
    $entity3->save();
    $this->assertInstanceOf(DailyWeatherForecast::class, $entity3);
    self::assertEquals(3, $entity3->id());

    $entity4 = $storage->create([
      'key' => 'forecast_st_gallen_2020_09_23',
      'averageTemperature' => 26.5
    ]);
    $entity4->save();
    $this->assertInstanceOf(DailyWeatherForecast::class, $entity4);
    self::assertEquals(4, $entity4->id());

    $allEntities = $storage->loadMultiple();
    self::assertCount(4, $allEntities);
    self::assertContainsOnlyInstancesOf(DailyWeatherForecast::class, $allEntities);

    $loadedEntity1 = $storage->load($entity1->id());
    /** @var \Drupal\cache_entity_type_example\Entity\DailyWeatherForecast $entity1 */
    $this->assertIsEntityEqual($entity1, $loadedEntity1);
    /** @var \Drupal\cache_entity_type_example\Entity\DailyWeatherForecast $entity2 */
    $loadedEntity2 = $storage->load($entity2->id());
    $this->assertIsEntityEqual($entity2, $loadedEntity2);

    $storage->delete([$entity2]);
    $loadedDeletedEntity2 = $storage->load($entity2->id());
    self::assertNull($loadedDeletedEntity2);

    $allEntitiesWithout2 = $storage->loadMultiple();
    self::assertCount(3, $allEntitiesWithout2);
    self::assertContainsOnlyInstancesOf(DailyWeatherForecast::class, $allEntitiesWithout2);

    $filledInEntity2 = $storage->create([
      'key' => 'forecast_st_gallen_2020_09_21_v2',
      'averageTemperature' => 24.8
    ]);
    $filledInEntity2->save();
    $this->assertInstanceOf(DailyWeatherForecast::class, $filledInEntity2);
    self::assertEquals(2, $filledInEntity2->id());

    $allEntitiesWithFilledIn = $storage->loadMultiple();
    self::assertCount(4, $allEntitiesWithFilledIn);
    self::assertContainsOnlyInstancesOf(DailyWeatherForecast::class, $allEntitiesWithFilledIn);

    $storage->delete($allEntitiesWithFilledIn);
    self::assertCount(0, $storage->loadMultiple());
  }

}
