<?php

declare(strict_types=1);

namespace Drupal\Tests\cache_entity_type\Kernel;

use Drupal\cache_entity_type_example\Api\WeeklyWeatherForecastApiClient;
use Drupal\cache_entity_type_example\Entity\WeeklyWeatherForecast;
use Drupal\cache_entity_type_example\Entity\WeeklyWeatherForecastEntityStorage;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Tests the WeeklyWeatherForecastEntityStorageTest class.
 *
 * This class implements the fallback loading functionality that is provided by the base storage.
 *
 * @package Drupal\Tests\cache_entity_type\Kernel
 * @coversDefaultClass \Drupal\cache_entity_type_example\Entity\WeeklyWeatherForecastEntityStorage
 * @group cache_entity_type
 */
class FallbackLoadingEntityStorageTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'cache_entity_type_example',
  ];

  private const ENTITY_TYPE_ID = 'example_weekly_weather_forecast';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema(self::ENTITY_TYPE_ID);

    $entityTypeManager = $this->container->get('entity_type.manager');
    if (!$entityTypeManager instanceof EntityTypeManagerInterface) {
      throw new ServiceNotFoundException('entity_type.manager');
    }
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Checks if the storage can be loaded.
   *
   * @covers ::createInstance
   */
  public function testStorageCanBeLoaded(): void {
    $storage = NULL;

    try {
      $storage = $this->entityTypeManager->getStorage(self::ENTITY_TYPE_ID);
    }
    catch (\Exception $exception) {
      $this->fail('Getting cache entity type storage failed.');
    }

    $this->assertInstanceOf(WeeklyWeatherForecastEntityStorage::class, $storage);
  }

  /**
   * Tests the fallback loading.
   */
  public function testFallbackLoading(): void {
    $storage = $this->entityTypeManager->getStorage(self::ENTITY_TYPE_ID);

    $entityId = 31;
    // Test if entity is loaded from fallback source (API).
    $loadedEntity = $storage->load($entityId);
    self::assertInstanceOf(WeeklyWeatherForecast::class, $loadedEntity);
    /** @var \Drupal\cache_entity_type_example\Entity\WeeklyWeatherForecast $loadedEntity */
    self::assertEquals(WeeklyWeatherForecastApiClient::MOCK_AVERAGE_TEMPERATURE, $loadedEntity->getAverageTemperature());
    self::assertEquals($entityId, $loadedEntity->id());

    // Test if the entity is loaded again from fallback source if we delete it.
    $storage->delete([$loadedEntity]);
    $loadedEntity = $storage->load($entityId);
    self::assertInstanceOf(WeeklyWeatherForecast::class, $loadedEntity);
    /** @var \Drupal\cache_entity_type_example\Entity\WeeklyWeatherForecast $loadedEntity */
    self::assertEquals(WeeklyWeatherForecastApiClient::MOCK_AVERAGE_TEMPERATURE, $loadedEntity->getAverageTemperature());
    self::assertEquals($entityId, $loadedEntity->id());
  }

}
