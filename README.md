# Cache Entity Type
Provides a base entity type and entity storage for storing entities in the cache instead of using the database.

Can be used for example when you want to temporarily store data that was retrieved by an API. To avoid calling that API all the time.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/cache_entity_type

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/cache_entity_type

## Requirements

* This module requires Drupal ^8.8 or ^9.
* PHP >= 7.4.
* No additional modules are needed.

## Usage
See the [cache_entity_type_example](./modules/cache_entity_type_example) module for examples.

Create an Entity class which uses the [CacheEntityStorage](./src/Entity/Cache/CacheEntityStorage.php) class or a subclass of it as storage.

Either [CacheEntityBase](./src/Entity/CacheEntityBase.php) or [ExtendedCacheEntityBase](./src/Entity/ExtendedCacheEntityBase.php) can be used as a base class. But don't have to. However the Entity Type **must** implement the [CacheEntityInterface](./src/Entity/CacheEntityInterface.php).

```php
use Drupal\cache_entity_type\Entity\CacheEntityBase;

/**
 * Class DailyWeatherForecast.
 *
 * @EntityType(
 *   id = "example_daily_weather_forecast",
 *   label = @Translation("Weather forecast for a single day"),
 *   handlers = {
 *     "storage" = "Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage",
 *   },
 *   render_cache = FALSE,
 *   entity_keys = {
 *     "id" = "id"
 *   },
 * )
 */
class DailyWeatherForecast extends CacheEntityBase {}
```

## Maintainers

**Current maintainers:**
 * Orlando Thöny - https://www.drupal.org/u/orlandothoeny

**This project has been sponsored by:**

 * **Namics** - Initial development

   Namics is one of the leading providers of e-business and digital brand
   communication services in the German-speaking region. The full-service agency
   helps companies transform business models with top-quality interdisciplinary
   solutions, promising increased, measurable success for their clients.
   Visit https://www.namics.com/en for more information.
