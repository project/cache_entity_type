<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type_example\Api;

/**
 * Simulates an API client.
 *
 * @package Drupal\cache_entity_type_example\Api
 */
class WeeklyWeatherForecastApiClient {

  /**
   * Example value for average temperature.
   */
  public const MOCK_AVERAGE_TEMPERATURE = 25.3;

  /**
   * Gets a weekly weather forecast.
   *
   * @param int $calendarWeek
   *   The calendar week of the forecast to get.
   *
   * @return array|null
   *   Array that represents a weekly forecast.
   *   Array keys:
   *    - calendarWeek
   *    - averageTemperature
   *   NULL on error.
   */
  public function getWeeklyForecast(int $calendarWeek): ?array {
    /* We don't actually call an API, return example data for demonstration purposes. */
    return [
      'calendarWeek' => $calendarWeek,
      'averageTemperature' => self::MOCK_AVERAGE_TEMPERATURE,
    ];
  }

}
