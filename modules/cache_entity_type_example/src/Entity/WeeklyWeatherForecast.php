<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type_example\Entity;

use Drupal\cache_entity_type\Entity\ExtendedCacheEntityBase;

/**
 * Class WeeklyWeatherForecast.
 *
 * @EntityType(
 *   id = "example_weekly_weather_forecast",
 *   label = @Translation("Weather forecast for a week"),
 *   handlers = {
 *     "storage" = "Drupal\cache_entity_type_example\Entity\WeeklyWeatherForecastEntityStorage",
 *   },
 *   render_cache = FALSE,
 *   entity_keys = {
 *     "id" = "id"
 *   },
 * )
 */
class WeeklyWeatherForecast extends ExtendedCacheEntityBase {

  /**
   * The average temperature in °C.
   *
   * @var float
   */
  protected float $averageTemperature;

  /**
   * The properties that must be set on object creation.
   */
  protected const REQUIRED_PROPERTIES = ['averageTemperature'];

  /**
   * WeeklyWeatherForecast constructor.
   *
   * @param array $values
   *   The ID represents the calendar week.
   *   Possible & required keys:
   *   - averageTemperature
   *   An array of values to set,
   *   keyed by property name.
   * @param string $entity_type
   *   The entity type ID.
   *
   * @throws \Drupal\cache_entity_type\Exception\RequiredEntityPropertiesMissingException
   *   Thrown if required entity properties are not set.
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);

    $this->averageTemperature = $values['averageTemperature'];
  }

  /**
   * Returns the average temperature.
   *
   * @return float
   *   The average temperature.
   */
  public function getAverageTemperature(): float {
    return $this->averageTemperature;
  }

}
