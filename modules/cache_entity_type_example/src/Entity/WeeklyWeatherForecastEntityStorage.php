<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type_example\Entity;

use Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage;
use Drupal\cache_entity_type\Exception\UnsupportedEntityTypeCacheStorageException;
use Drupal\cache_entity_type_example\Api\WeeklyWeatherForecastApiClient;
use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Storage for WeeklyWeatherForecastEntityStorage entities.
 *
 * @package Drupal\cache_entity_type_example\Entity
 */
class WeeklyWeatherForecastEntityStorage extends CacheEntityStorage {

  /**
   * The API client.
   *
   * @var \Drupal\cache_entity_type_example\Api\WeeklyWeatherForecastApiClient
   */
  protected WeeklyWeatherForecastApiClient $apiClient;

  /**
   * MortgageInterestRateEntityStorage constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type.
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cacheFactory
   *   The cache factory.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memoryCache
   *   The memory cache.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   The cache tags invalidator.
   * @param \Drupal\cache_entity_type_example\Api\WeeklyWeatherForecastApiClient $apiClient
   *   The API client.
   */
  public function __construct(EntityTypeInterface $entityType, CacheFactoryInterface $cacheFactory, MemoryCacheInterface $memoryCache, LoggerChannelFactoryInterface $loggerChannelFactory, CacheTagsInvalidatorInterface $cacheTagsInvalidator, WeeklyWeatherForecastApiClient $apiClient) {
    parent::__construct($entityType, $cacheFactory, $memoryCache, $loggerChannelFactory, $cacheTagsInvalidator);

    $this->apiClient = $apiClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('cache_factory'),
      $container->get('entity.memory_cache'),
      $container->get('logger.factory'),
      $container->get('cache_tags.invalidator'),
      new WeeklyWeatherForecastApiClient()
    );
  }

  /**
   * Checks if given entity is supported by this storage handler.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  protected function isSupportedEntityType(EntityInterface $entity): bool {
    $isSupportedByParentStorage = parent::isSupportedEntityType($entity);
    if (!$isSupportedByParentStorage) {
      return FALSE;
    }

    if ($entity instanceof WeeklyWeatherForecast) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Creates a new entity.
   *
   * @param int $id
   *   The entity ID.
   * @param float $averageTemperature
   *   The average temperature.
   *
   * @return \Drupal\cache_entity_type_example\Entity\WeeklyWeatherForecast
   *   The created entity.
   *
   * @throws \Drupal\cache_entity_type\Exception\UnsupportedEntityTypeCacheStorageException
   *   If the type of the created entity is wrong.
   */
  protected function createEntity(int $id, float $averageTemperature): WeeklyWeatherForecast {
    $entity = $this->create([
      WeeklyWeatherForecast::ID_KEY => $id,
      'averageTemperature' => $averageTemperature
    ]);

    if (!$this->isSupportedEntityType($entity)) {
      throw new UnsupportedEntityTypeCacheStorageException();
    }

    /** @var \Drupal\cache_entity_type_example\Entity\WeeklyWeatherForecast $entity */
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function shouldLogFallbackEntityLoadingFailure(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\cache_entity_type\Exception\UnsupportedEntityTypeCacheStorageException
   *   If the type of the created entity is wrong.
   */
  protected function loadMissingEntitiesFromFallbackSource(array $entityIds = NULL): array {
    $entities = [];

    foreach ($entityIds as $entityId) {
      $endpointResult = $this->apiClient->getWeeklyForecast($entityId);
      if ($endpointResult === NULL) {
        $this->logger->error('Could not load weekly weather forecast from API.');
        return [];
      }

      $entities[$entityId] = $this->createEntity($endpointResult['calendarWeek'], $endpointResult['averageTemperature']);
    }

    return $entities;
  }

}
