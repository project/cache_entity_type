<?php

declare(strict_types=1);

namespace Drupal\cache_entity_type_example\Entity;

use Drupal\cache_entity_type\Entity\ExtendedCacheEntityBase;

/**
 * Class DailyWeatherForecast.
 *
 * @EntityType(
 *   id = "example_daily_weather_forecast",
 *   label = @Translation("Weather forecast for a single day"),
 *   handlers = {
 *     "storage" = "Drupal\cache_entity_type\Entity\Cache\CacheEntityStorage",
 *   },
 *   render_cache = FALSE,
 *   entity_keys = {
 *     "id" = "id"
 *   },
 * )
 */
class DailyWeatherForecast extends ExtendedCacheEntityBase {

  /**
   * The key that identifies this day.
   *
   * @var string
   */
  protected string $key;

  /**
   * The average temperature in °C.
   *
   * @var float
   */
  protected float $averageTemperature;

  /**
   * The properties that must be set on object creation.
   */
  protected const REQUIRED_PROPERTIES = ['key', 'averageTemperature'];

  /**
   * DailyWeatherForecast constructor.
   *
   * @param array $values
   *   Possible & required keys:
   *   - key
   *   - averageTemperature
   *   An array of values to set,
   *   keyed by property name.
   * @param string $entity_type
   *   The entity type ID.
   *
   * @throws \Drupal\cache_entity_type\Exception\RequiredEntityPropertiesMissingException
   *   Thrown if required entity properties are not set.
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);

    $this->key = $values['key'];
    $this->averageTemperature = $values['averageTemperature'];
  }

  /**
   * Returns the key.
   *
   * @return string
   *   The key.
   */
  public function getKey(): string {
    return $this->key;
  }

  /**
   * Returns the average temperature.
   *
   * @return float
   *   The average temperature.
   */
  public function getAverageTemperature(): float {
    return $this->averageTemperature;
  }

}
